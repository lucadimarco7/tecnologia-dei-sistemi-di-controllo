#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);
#define GAME D0
#define MUSIC D4

// Wifi network station credentials
#define WIFI_SSID "iPhone di Antonino"
#define WIFI_PASSWORD "Gpimlmslp0k3spot."
// Telegram BOT Token (Get from Botfather)
#define BOT_TOKEN "2107551400:AAHXB6MHM2NqUOZIHQO_4BFI-NB5xsgC4mk"

const unsigned long BOT_MTBS = 1000; // mean time between scan messages
int timegame=69;
X509List cert(TELEGRAM_CERTIFICATE_ROOT);
WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOT_TOKEN, secured_client);
unsigned long bot_lasttime; // last time messages' scan has been done

void handleNewMessages(int numNewMessages)
{
  Serial.print("handleNewMessages ");
  Serial.println(numNewMessages);

  for (int i = 0; i < numNewMessages; i++)
  {
    String chat_id = bot.messages[i].chat_id;
    String text = bot.messages[i].text;

    String from_name = bot.messages[i].from_name;
    if (from_name == "")
      from_name = "Guest";

    if (text == "/newgame")
    {
      bot.sendMessage(chat_id, "Gioco avviato", "");
      digitalWrite(GAME, HIGH); 
      countdown();
      
      
      
      
     
    }


    if (text == "/start")
    {
      String welcome = "Welcome in the Basket game " + from_name + ".\n";
      welcome += "This is Flash Led Bot example.\n\n";
      welcome += "/newgame : for start a new game\n";
      bot.sendMessage(chat_id, welcome, "Markdown");
    }
  }
}


void setup()
{
  Serial.begin(115200);
  Serial.println();
  lcd.init();                      // initialize the lcd 
 delay(10);
  lcd.backlight();
  pinMode(GAME, OUTPUT); // initialize digital ledPin as an output.

  delay(10);
  digitalWrite(GAME, LOW); // initialize pin as off (active LOW)

  // attempt to connect to Wifi network:
  configTime(0, 0, "pool.ntp.org");      // get UTC time via NTP
  secured_client.setTrustAnchors(&cert); // Add root certificate for api.telegram.org
  Serial.print("Connecting to Wifi SSID ");
  Serial.print(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.print("\nWiFi connected. IP address: ");
  Serial.println(WiFi.localIP());

  // Check NTP/Time, usually it is instantaneous and you can delete the code below.
  Serial.print("Retrieving time: ");
  time_t now = time(nullptr);
  while (now < 24 * 3600)
  {
    Serial.print(".");
    delay(100);
    now = time(nullptr);
  }
  Serial.println(now);
}

void countdown(){
    lcd.setBacklight(255);
    lcd.home();
    lcd.clear();
  int i=timegame;
  while(i>0){
    i--;
    if(i==9)   lcd.clear(); 
       lcd.setCursor(0, 0); lcd.print("Time: ");
       lcd.setCursor(7,0); lcd.print(i);
    delay(1000);
  }
  lcd.clear();
  digitalWrite(GAME, LOW);
    lcd.setCursor(0, 0); lcd.print("Time expired!");
}

  void welcome(){
    lcd.setBacklight(255);
    lcd.home();
  lcd.print("Telegram Basket");
   lcd.setCursor(7,1); 
  lcd.print("Game ");
}

void loop()
{  welcome();
  if (millis() - bot_lasttime > BOT_MTBS)
  {
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

    while (numNewMessages)
    {
      Serial.println("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }

    bot_lasttime = millis();
  }
}
