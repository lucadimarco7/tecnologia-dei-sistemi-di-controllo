#include <LiquidCrystal.h>
#include <Servo.h>
#define P1 7
#define P2 2
#define P3 3
LiquidCrystal lcd (4, 5, 10, 11, 12, 13);
int statePIR=LOW;
int state2=LOW;
int state3=LOW;
int state_game=LOW;
int pir_reader;
int score=0;
Servo myservo;


void setup() 
{  
  pinMode(8, INPUT);
  pinMode(P3,INPUT);
  pinMode(P2,INPUT); 
  pinMode(P1,INPUT);                  
  lcd.begin(16, 2);     
  Serial.begin(9600);
  myservo.attach(6);
  myservo.write(90);
}

void gameStartMessage(){
  lcd.setCursor(5,0);             
  lcd.print("Game");             
  lcd.setCursor(5,2);              
  lcd.print("Start!!");  
  delay(2000);
  lcd.clear();
  Serial.println("Game start! ");
}
 
void Message(){
  lcd.setCursor(2,0);             
  lcd.print("Use telegram ");             
  lcd.setCursor(2,1);              
  lcd.print("to start!");  
  delay(1000);
  lcd.clear();    
  
}

void gameStart(){
  score=0;
  gameStartMessage();
 displayScore();
  while(digitalRead(8)==HIGH){
    int val_1 = digitalRead(P1);
    int val_2=digitalRead(P2);
    int val_3=digitalRead(P3);

if(val_1==HIGH){
  if (statePIR == LOW) {
      statePIR = HIGH;
      score++;
      delay(200);
      displayScore();
    }    
}
 else if (statePIR == HIGH) {
      statePIR = LOW;   
  }
    
      
 if(val_2==HIGH){
  if (state2 == LOW) {
      state2 = HIGH;
      score=score+2;
      delay(200);
      displayScore();
    }    
}
 else if (state2 == HIGH) {
      state2 = LOW;   
  }
       
  
if(val_3==HIGH){
  if (state3 == LOW) {
      delay(200);
      state3 = HIGH;
      score=score+3;
      displayScore();
    }    
}
 else if (state3 == HIGH) {
      state3 = LOW;   
  }
}
closeBasket();
displayScoreEnd();              
}

void openBasket(){
  myservo.write(0);
}

void closeBasket(){
  myservo.write(90);
}
 
void displayScore(){
 Serial.println(scoreString(score));  
  lcd.setCursor(2,0);             
  lcd.print(scoreString(score)); 
 
}

 
void displayScoreEnd(){
 Serial.println(scoreString(score));  
  lcd.setCursor(2,0);             
  lcd.print("END!Your score is"); 
  lcd.setCursor(2,1);              
  lcd.print(score); 
  lcd.print(" points");
  delay(5000);
  lcd.clear();  
}

String scoreString(int score){
  String scorestring="Score:"+ String(score) + " points";
  return scorestring;
  
}

 
void loop()
{
 Serial.println(digitalRead(8));
state_game=digitalRead(8);
  if(state_game==HIGH)
  {
   //servo aperto
  openBasket();
  gameStart();
  }
  else{
    //servo chiuso
    closeBasket();
   Message();
  }
}
